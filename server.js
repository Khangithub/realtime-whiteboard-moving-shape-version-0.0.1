var express = require('express');
var app = express();
var http = require('http').createServer(app);
var io = require('socket.io')(http);
var path = require('path');

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');
});

io.on('connection', function (socket) {
    console.log(socket.id + ' was connected');
    socket.on('disconnect', function () {
        console.log(socket.id + ' was disconnected');
    });
});

io.on('connection', function (socket) {
    socket.on('object_moving', function (obj) {
        socket.broadcast.emit('object_moving', obj);
    });

    socket.on('object_rotating', function (obj) {    
        socket.broadcast.emit('object_moving', obj);
    });

    socket.on('object_scaling', function (obj) {
        socket.broadcast.emit('object_moving', obj);
    });
    
    socket.on('who_is_moving', function (data) {
        socket.broadcast.emit('who_is_moving', data);
    })
});

http.listen(4000, function () {
    console.log('listening on *:4000');
});
