var canvas = this.__canvas = new fabric.Canvas('c');
var socket = io();
var rectContent = "";
var canvasList = document.getElementById('canvas_list');
var alert = document.getElementById('alert'); 

var rect = new fabric.Rect({
    left: 100,
    top: 100,
    fill: 'green',
    width: 60,
    height: 80,
    angle: 45
});

rect.id = 111;

var triangle = new fabric.Triangle({
    width: 20,
    height: 30,
    fill: 'blue',
    left: 150,
    top: 150
});

triangle.id = 112;

var circle = new fabric.Circle({
    radius: 30,
    fill: 'purple',
    left: 200,
    top: 200
});

circle.id = 113;

canvas.add(rect, triangle, circle);

function resetOrdination(oldObj, newObj) {
    oldObj.left = newObj.left;
    oldObj.top = newObj.top;
    oldObj.angle = newObj.angle;
    oldObj.height = newObj.height;
    oldObj.width = newObj.width;
    oldObj.scaleX = newObj.scaleX;
    oldObj.scaleY = newObj.scaleY;
};

canvas.on('object:moving', function () {
    var selectedObject = canvas.getActiveObject();
    var convertObjToJSON = JSON.stringify(selectedObject);
    var obj = {
        id: selectedObject.id,
        msg: convertObjToJSON
    }
    socket.emit('object_moving', obj);

});

canvas.on('object:rotating', function(){
    var selectedObject = canvas.getActiveObject();
    console.log(selectedObject);
    var convertObjToJSON = JSON.stringify(selectedObject);
    var obj = {
        id: selectedObject.id,
        msg: convertObjToJSON
    }
    socket.emit('object_rotating', obj);
});

canvas.on('object:scaling', function(){
    var selectedObject = canvas.getActiveObject();
    var convertObjToJSON = JSON.stringify(selectedObject);
    var obj = {
        id: selectedObject.id,
        msg: convertObjToJSON
    }
    socket.emit('object_scaling', obj);
})
// listen for events

socket.on('object_moving', function (obj) {
    var objArr = canvas.getObjects();
    var newObj = JSON.parse(obj.msg);
    var index;
    for (var i = 0; i < objArr.length; i++) {
        if (objArr[i].id === obj.id) {
            index = i;
        }
    }
    resetOrdination(objArr[index], newObj);
    canvas.renderAll();
});


socket.on('who_is_moving', function (msg) {
    alert.innerHTML = msg + ' is moving';
});